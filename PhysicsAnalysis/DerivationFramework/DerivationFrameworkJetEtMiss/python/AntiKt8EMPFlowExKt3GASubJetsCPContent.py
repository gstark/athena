# Copyright (C) 2002-2020 CERN for the benefit of the ATLAS collaboration

AntiKt8EMPFlowExKt3GASubJetsCPContent = [
"AntiKt8EMPFlowExKt3GASubJets",
"AntiKt8EMPFlowExKt3GASubJetsAux.pt.eta.phi.m.GhostBHadronsFinal.GhostCHadronsFinal",
]

# Copyright (C) 2002-2020 CERN for the benefit of the ATLAS collaboration

AntiKt8EMPFlowExKt3SubJetsCPContent = [
"AntiKt8EMPFlowExKt3SubJets",
"AntiKt8EMPFlowExKt3SubJetsAux.pt.eta.phi.m.GhostBHadronsFinal.GhostCHadronsFinal",
]

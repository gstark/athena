# Copyright (C) 2002-2020 CERN for the benefit of the ATLAS collaboration

AntiKt8EMPFlowExKt2SubJetsCPContent = [
"AntiKt8EMPFlowExKt2SubJets",
"AntiKt8EMPFlowExKt2SubJetsAux.pt.eta.phi.m.GhostBHadronsFinal.GhostCHadronsFinal",
]

#include "GaudiKernel/DeclareFactoryEntries.h"
#include "DerivationFrameworkHDBS/SkimmingToolHDBS2.h"

using namespace DerivationFramework;

DECLARE_TOOL_FACTORY( SkimmingToolHDBS2 )

DECLARE_FACTORY_ENTRIES( DerivationFrameworkHDBS ) {
   DECLARE_TOOL( SkimmingToolHDBS2 )
}


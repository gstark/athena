# Copyright (C) 2002-2017 CERN for the benefit of the ATLAS collaboration

EXOT13SmartContent = [
    "InDetTrackParticles",
    "Electrons",
    "Muons",
    "PrimaryVertices",
    "MET_Reference_AntiKt4EMTopo",
    "BTagging_AntiKt4EMTopo_201810",
    "AntiKt4EMTopoJets_BTagging201810",
    "AntiKt4EMTopoJets",
    "AntiKt4TruthJets"
]

EXOT13AllVariablesContent = [
    "MuonSpectrometerTrackParticles",
    "ExtrapolatedMuonTrackParticles",
    "METAssoc_AntiKt4EMTopo",
    "MET_Core_AntiKt4EMTopo",
    "MET_Truth",
    "TruthEvents",
    "TruthVertices",
    "TruthParticles",
    "HLT_xAOD__JetContainer_SplitJet"
]

EXOT13ExtraVariables = [
    "AntiKt4EMTopoJets.HECQuality.sumpttrk.FracSamplingMax.NegativeE.AverageLArQF.FracSamplingMaxIndex.LArQuality.HECFrac.EMFrac.Width.Timing.btagging.btaggingLink.MV2c10_discriminant.MV2c20_discriminant.BTagTrackToJetAssociator.BTagTrackToJetAssociatorBB",
    "Muons.allAuthors.rpcHitTime.rpcHitIdentifier.rpcHitPositionX.rpcHitPositionY.rpcHitPositionZ",
    "Electrons.MediumLH.ptvarcone30.Reta.Rphi.Rhad1.Rhad.weta2.Eratio.f3.wtots1.deltaEta1.deltaPhiRescaled2",
    "MuonSegments.x.y.z.px.py.pz.chamberIndex",
    "HLT_xAOD__JetContainer_a4tcemsubjesFS.m.EMFrac",
]

EXOT13UnslimmedContent = []

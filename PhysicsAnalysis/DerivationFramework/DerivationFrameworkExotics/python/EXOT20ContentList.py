# Copyright (C) 2002-2017 CERN for the benefit of the ATLAS collaboration

EXOT20SmartCollections = [
    "Muons",
    "AntiKt4EMTopoJets",
    "AntiKt4EMTopoJets_BTagging201810",
    "BTagging_AntiKt4EMTopo_201810",
    "MET_Reference_AntiKt4EMTopo",
    "InDetTrackParticles",
    "PrimaryVertices"
]

EXOT20AllVariables = [
    "TruthVertices",
    "TruthParticles",
    "MSonlyTracklets",
    "MuonSpectrometerTrackParticles",
    "ExtrapolatedMuonTrackParticles",
    "CombinedMuonTrackParticles"
]

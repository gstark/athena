# Copyright (C) 2002-2017 CERN for the benefit of the ATLAS collaboration

EXOT28SmartContent = [
    "InDetTrackParticles",
    "Electrons",
    "Muons",
    "PrimaryVertices",
    "MET_Reference_AntiKt4EMPFlow",
    "AntiKt4EMPFlowJets",
    "AntiKt4EMPFlowJets_BTagging201810",
    "AntiKt4EMPFlowJets_BTagging201903",
    "AntiKt4TruthJets",
    "BTagging_AntiKt4EMPFlow_201810",
    "BTagging_AntiKt4EMPFlow_201903",
]

EXOT28AllVariablesContent = [
    "METAssoc_AntiKt4EMPFlow",
    "MET_Core_AntiKt4EMPFlow",
    "MET_Truth",
    "TruthEvents",
    "TruthVertices",
    "TruthParticles",
    "MuonTruthParticles",
    "ElectronTruthParticles"
]

EXOT28ExtraVariables = []

EXOT28UnslimmedContent = []


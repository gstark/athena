# Copyright (C) 2002-2020 CERN for the benefit of the ATLAS collaboration

from VRTrackJetCPContent import getVRTrackJetCPContent as getContent

AntiKtVR30Rmax4Rmin02TrackJets_BTagging201810CPContent = getContent(
    "AntiKtVR30Rmax4Rmin02TrackJets_BTagging201810")

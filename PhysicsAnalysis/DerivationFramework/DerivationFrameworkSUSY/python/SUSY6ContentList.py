# Copyright (C) 2002-2018 CERN for the benefit of the ATLAS collaboration

SUSY6ExtraVariables = [
  "AntiKt4EMTopoJets.NumTrkPt1000.TrackWidthPt1000.NumTrkPt500.DFCommonJets_jetClean_VeryLooseBadLLP.DFCommonJets_jetClean_SuperLooseBadLLP",
  "AntiKt4TruthJets.eta.m.phi.pt.TruthLabelDeltaR_B.TruthLabelDeltaR_C.TruthLabelDeltaR_T.TruthLabelID.ConeTruthLabelID.PartonTruthLabelID.HadronConeExclTruthLabelID",
  # "CombinedMuonTrackParticles.d0.z0.vz.definingParametersCovMatrix.truthOrigin.truthType",
  "ExtrapolatedMuonTrackParticles.d0.z0.vz.definingParametersCovMatrix.truthOrigin.truthType",
  "InDetTrackParticles.TRTdEdx.TRTdEdxUsedHits.hitPattern.numberOfContribPixelLayers.numberOfGangedFlaggedFakes.numberOfIBLOverflowsdEdx.numberOfPixelOutliers.numberOfPixelSplitHits.numberOfPixelSpoiltHits.numberOfSCTOutliers.numberOfSCTSpoiltHits.numberOfTRTDeadStraws.numberOfTRTHits.numberOfTRTHoles.numberOfTRTOutliers.numberOfTRTSharedHits.numberOfUsedHitsdEdx.pixeldEdx.truthMatchProbability.truthOrigin.truthType",
  "InDetTrackParticles.chiSquared.d0.definingParametersCovMatrix.expectInnermostPixelLayerHit.expectNextToInnermostPixelLayerHit.numberDoF.numberOfInnermostPixelLayerHits.numberOfNextToInnermostPixelLayerHits.numberOfPixelDeadSensors.numberOfPixelHits.numberOfPixelHoles.numberOfPixelSharedHits.numberOfSCTDeadSensors.numberOfSCTHits.numberOfSCTHoles.numberOfSCTSharedHits.phi.qOverP.theta.z0",
  "InDetTrackParticles.truthParticleLink.vertexLink.vz",
  "InDetTrackParticles.ptcone20.ptcone30.ptcone40.ptvarcone20.ptvarcone30.ptvarcone40.topoetcone20.topoetcone30.topoetcone40.topoetcone20NonCoreCone.topoetcone30NonCoreCone.topoetcone40NonCoreCone",
  "InDetTrackParticles.TrkIsoPtPdEdx_ptcone20.TrkIsoPtPdEdx_ptcone30.TrkIsoPtTightPdEdx_ptcone20.TrkIsoPtTightPdEdx_ptcone30",
  "InDetTrackParticles.SUSY6_CaloCelldEta.SUSY6_CaloCelldPhi.SUSY6_CaloCelldR.SUSY6_CaloCelldX.SUSY6_CaloCelldY.SUSY6_CaloCelldZ.SUSY6_CaloCellE.SUSY6_CaloCellEta.SUSY6_CaloCellGain.SUSY6_CaloCellID.SUSY6_CaloCellPhi.SUSY6_CaloCellProvenance.SUSY6_CaloCellQuality.SUSY6_CaloCellR.SUSY6_CaloCellSampling.SUSY6_CaloCellTime.SUSY6_CaloCellX.SUSY6_CaloCellY.SUSY6_CaloCellZ",
  "Muons.CaloLRLikelihood.CaloMuonIDTag.charge.InnerDetectorPt.MuonSpectrometerPt.ptcone20.ptcone30.quality",
  "MuonTruthParticles.barcode.decayVtxLink.e.m.pdgId.prodVtxLink.px.py.pz.recoMuonLink.status.truthOrigin.truthType",
  "Photons.author.Loose.Tight",
  "TauJets.IsTruthMatched.truthParticleLink.truthOrigin.truthType.truthJetLink.DFCommonTausLoose",
  #"TauJets.pt.eta.phi.m.IsTruthMatched.pt_vis.truthParticleLink.truthOrigin.truthType.truthJetLink.DFCommonTausLoose",
  # QUESTION SUSY8 had TauJet extra variables, not sure they were necessary
]

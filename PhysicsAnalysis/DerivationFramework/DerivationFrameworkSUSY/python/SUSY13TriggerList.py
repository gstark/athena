# Copyright (C) 2002-2017 CERN for the benefit of the ATLAS collaboration

triggersNavThin = [
'HLT_g300_etcut',
'HLT_g140_loose',
'HLT_g120_loose',
'HLT_g60_loose_xe60noL1',
'HLT_g70_loose_xe70noL1',
'HLT_g75_tight_3j50noL1_L1EM22VHI',
'HLT_g140_tight',
'HLT_g160_loose',
'HLT_g200_loose',
]

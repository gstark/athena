SUSY19ExtraVariables = [
    "AntiKt4EMTopoJets.NumTrkPt1000.TrackWidthPt1000.NumTrkPt500.DFCommonJets_jetClean_VeryLooseBadLLP",
    "Muons.ptcone30.ptcone20.charge.quality.InnerDetectorPt.MuonSpectrometerPt.CaloLRLikelihood.CaloMuonIDTag.eta_sampl.phi_sampl",
    "AntiKt4TruthJets.eta.m.phi.pt.TruthLabelDeltaR_B.TruthLabelDeltaR_C.TruthLabelDeltaR_T.TruthLabelID.ConeTruthLabelID.PartonTruthLabelID",
    "InDetTrackParticles.TRTdEdx.TRTdEdxUsedHits.hitPattern.numberOfContribPixelLayers.numberOfGangedFlaggedFakes.numberOfIBLOverflowsdEdx.numberOfPixelOutliers.numberOfPixelSplitHits.numberOfPixelSpoiltHits.numberOfSCTOutliers.numberOfSCTSpoiltHits.numberOfTRTDeadStraws.numberOfTRTHits.numberOfTRTHoles.numberOfTRTOutliers.numberOfTRTSharedHits.numberOfUsedHitsdEdx.pixeldEdx.truthMatchProbability.truthOrigin.truthType",
    "InDetTrackParticles.chiSquared.d0.definingParametersCovMatrix.expectInnermostPixelLayerHit.expectNextToInnermostPixelLayerHit.numberDoF.numberOfInnermostPixelLayerHits.numberOfNextToInnermostPixelLayerHits.numberOfPixelDeadSensors.numberOfPixelHits.numberOfPixelHoles.numberOfPixelSharedHits.numberOfSCTDeadSensors.numberOfSCTHits.numberOfSCTHoles.numberOfSCTSharedHits.phi.qOverP.theta.z0",
    "InDetTrackParticles.truthParticleLink.vertexLink.vz",
    "InDetTrackParticles.ptcone20.ptcone30.ptcone40.ptvarcone20.ptvarcone30.ptvarcone40.topoetcone20.topoetcone30.topoetcone40.topoetcone20NonCoreCone.topoetcone30NonCoreCone.topoetcone40NonCoreCone",
    "BTagging_AntiKt4EMTopo.MV1_discriminant.MV1c_discriminant",
    "AntiKt2PV0TrackJets.eta.m.phi.pt.btagging.btaggingLink",
    "BTagging_AntiKt2Track.MV2c10_discriminant",
    "InDetTrackParticles.trackCaloClusEta.trackCaloClusPhi.trackCaloSampleE.trackCaloSampleNumber",
    ]

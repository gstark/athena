# Copyright (C) 2002-2017 CERN for the benefit of the ATLAS collaboration

SUSY8JetMETTriggers = [
  'HLT_j100_xe80',
  'HLT_j80_xe80'
]

SUSY8MuonTriggers = [
  'HLT_mu20.*',
  'HLT_mu24.*',
  'HLT_mu26.*',
  'HLT_mu40.*',
  'HLT_mu50.*',
  'HLT_mu60.*'
]

SUSY8DimuonTriggers = [
  'HLT_2mu.*'
]

SUSY8LateMuonTriggers = [
  'HLT_mu10_mgonly_L1LATEMU10_J50',
  'HLT_mu10_mgonly_L1LATEMU10_XE50',
  'HLT_mu10_mgonly_L1LATE-MU10_XE40'
]

SUSY8METTriggers = [
  'HLT_xe35.*',
  'HLT_xe50.*',
  'HLT_xe60.*',
  'HLT_xe70.*',
  'HLT_xe80.*',
  'HLT_xe90.*',
  'HLT_xe100.*',
  'HLT_xe110.*',
  'HLT_xe120.*'
]

# from DerivationFrameworkSUSY.SUSYCommonTriggerList import MET_2018, MET_2017, MET_2016, MET_2015
# SUSY8MuonTriggers = [
#     'HLT_mu26_ivarmedium',
#     'HLT_mu28_ivarmedium',
#     'HLT_mu60',
#     'HLT_mu60_0eta105_msonly',
#     'HLT_mu60_msonly_3layersEC',
#     'HLT_mu80',
#     'HLT_mu80_msonly_3layersEC',
# 
#     'HLT_mu26_ivarmedium',
#     'HLT_mu50',
#     'HLT_mu60',
#     'HLT_mu60_0eta105_msonly',
# 
#     'HLT_mu24_iloose',
#     'HLT_mu24_iloose_L1MU15',
#     'HLT_mu24_ivarloose',
#     'HLT_mu24_ivarloose_L1MU15',
#     'HLT_mu24_ivarmedium',
#     'HLT_mu24_imedium',
#     'HLT_mu26_ivarmedium',
#     'HLT_mu26_imedium',
#     'HLT_mu40',
#     'HLT_mu50',
# 
#     'HLT_mu20_iloose_L1MU15',
#     'HLT_mu40',
#     'HLT_mu60_0eta105_msonly',
# ]
# SUSY8DimuonTriggers = [
#     'HLT_2mu14',
#     'HLT_2mu15_L12MU10',
#     'HLT_mu24_mu12noL1',
#     'HLT_mu24_mu10noL1',
#     'HLT_mu24_mu8noL1',
#     'HLT_mu26_mu10noL1',
#     'HLT_mu26_mu8noL1',
#     'HLT_mu28_mu8noL1',
# 
#     'HLT_2mu14',
#     'HLT_mu22_mu8noL1',
#     'HLT_mu22_mu8noL1_calotag_0eta010',
#     'HLT_mu24_mu8noL1',
#     'HLT_mu24_mu8noL1_calotag_0eta010',
#     'HLT_mu26_mu10noL1',
# 
#     'HLT_mu20_mu8noL1',
#     'HLT_mu20_nomucomb_mu6noL1_nscan03',
#     'HLT_mu20_msonly_mu10noL1_msonly_nscan05_noComb',
#     'HLT_mu22_mu8noL1',
# 
#     'HLT_2mu10',
#     'HLT_mu18_mu8noL1',
# ]
# SUSY8METTriggers = MET_2018 + MET_2017 + MET_2016 + MET_2015

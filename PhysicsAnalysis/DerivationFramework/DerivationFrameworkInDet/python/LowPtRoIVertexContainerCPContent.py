# Copyright (C) 2002-2020 CERN for the benefit of the ATLAS collaboration 

LowPtRoIVertexContainerCPContent = [
"LowPtRoIVertexContainer",
"LowPtRoIVertexContainerAux.z.IsHS.boundary_high.boundary_low.perigee_z0_lead.perigee_z0_sublead"
]

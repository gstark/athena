// Dear emacs, this is -*- c++ -*-

/*
  Copyright (C) 2002-2017 CERN for the benefit of the ATLAS collaboration
*/
#warning "The IMuonSelectionTool has moved to MuonAnalysisInterfaces/IMuonSelectionTool.h . Please update your code."
#include <MuonAnalysisInterfaces/IMuonSelectionTool.h>

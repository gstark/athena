/*
  Copyright (C) 2002-2020 CERN for the benefit of the ATLAS collaboration
*/

#ifndef PARTICLEJETTOOLSDICT_H
#define PARTICLEJETTOOLSDICT_H

#include "ParticleJetTools/CopyTruthParticles.h"
#include "ParticleJetTools/CopyBosonTopLabelTruthParticles.h"
#include "ParticleJetTools/CopyTruthPartons.h"
#include "ParticleJetTools/CopyFlavorLabelTruthParticles.h"
#include "ParticleJetTools/CopyTruthJetParticles.h"
#include "ParticleJetTools/ParticleJetDeltaRLabelTool.h"
#include "ParticleJetTools/ParticleJetGhostLabelTool.h"
#include "ParticleJetTools/JetConeLabeling.h"
#include "ParticleJetTools/JetQuarkLabel.h"
#include "ParticleJetTools/JetPartonTruthLabel.h"
#include "ParticleJetTools/JetParticleAssociation.h"
#include "ParticleJetTools/JetParticleCenterOfMassAssociation.h"
#include "ParticleJetTools/JetParticleShrinkingConeAssociation.h"
#include "ParticleJetTools/JetParticleFixedConeAssociation.h"
#include "ParticleJetTools/JetTruthLabelingTool.h"

#ifndef XAOD_STANDALONE
#include "src/JetModifierAlg.h"
#include "src/JetAssocConstAlg.h"
#endif

#endif

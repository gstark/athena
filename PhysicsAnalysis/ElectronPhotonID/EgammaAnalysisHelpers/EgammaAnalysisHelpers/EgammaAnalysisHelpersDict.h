/*
  Copyright (C) 2002-2017 CERN for the benefit of the ATLAS collaboration
*/

#ifndef __EGAMMAANALYSISHELPERSDICT__
#define __EGAMMAANALYSISHELPERSDICT__

#include "EgammaAnalysisHelpers/PhotonHelpers.h"
#include "EgammaAnalysisHelpers/AsgElectronPhotonIsEMSelectorConfigHelper.h"

#endif

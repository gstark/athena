/*
  Copyright (C) 2002-2017 CERN for the benefit of the ATLAS collaboration
*/

// $Id: $

// Gaudi/Athena include(s):
#include "GaudiKernel/LoadFactoryEntries.h"

// Declare the library to the framework:
LOAD_FACTORY_ENTRIES( BPhysTools )

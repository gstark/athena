/*
  Copyright (C) 2002-2017 CERN for the benefit of the ATLAS collaboration
*/

#ifndef EGAMMAMVACALIB_EGAMMAMVACALIBDICT_H

#include "egammaMVACalib/egammaMVACalib.h"
#include "egammaMVACalib/IegammaMVATool.h"
#include "egammaMVACalib/egammaMVATool.h"

#endif // EGAMMAMVACALIB_EGAMMAMVACALIBDICT_H

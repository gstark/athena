/*
  Copyright (C) 2002-2020 CERN for the benefit of the ATLAS collaboration
*/


#ifndef JETRECLUSTERING_JETRECLUSTERINGDICT_H
#define JETRECLUSTERING_JETRECLUSTERINGDICT_H

#include "JetReclustering/EffectiveRTool.h"
#include "JetReclustering/JetReclusteringTool.h"

#endif

/*
  Copyright (C) 2002-2017 CERN for the benefit of the ATLAS collaboration
*/

#ifndef IJETJVTEFFICIENCY_H_
#define IJETJVTEFFICIENCY_H_

#include "JetAnalysisInterfaces/IJetJvtEfficiency.h"
#pragma message "This header has been moved to the JetAnalysisInterfaces package. Please update your includes to point here."

#endif /* IJETJVTEFFICIENCY_H_ */

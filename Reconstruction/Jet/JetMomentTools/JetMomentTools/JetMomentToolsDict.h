/*
  Copyright (C) 2002-2019 CERN for the benefit of the ATLAS collaboration
*/

#ifndef JETMOMENTTOOLS_JETMOMENTTOOLSDICT_H
#define JETMOMENTTOOLS_JETMOMENTTOOLSDICT_H

// #ifndef XAOD_STANDALONE
// #include "JetMomentTools/JetBadChanCorrTool.h"
// #include "JetMomentTools/JetIsolationTool.h"
// #include "JetMomentTools/JetVoronoiDiagramHelpers.h"
// #include "JetMomentTools/JetVoronoiMomentsTool.h"
// #endif

#include "JetMomentTools/JetOriginCorrectionTool.h"
#include "JetMomentTools/JetCaloEnergies.h"
#include "JetMomentTools/JetCaloQualityTool.h"
#include "JetMomentTools/JetPtAssociationTool.h"
#include "JetMomentTools/JetTrackMomentsTool.h"
#include "JetMomentTools/JetClusterMomentsTool.h"
#include "JetMomentTools/JetTrackSumMomentsTool.h"
#include "JetMomentTools/JetConstitFourMomTool.h"
#include "JetMomentTools/JetVertexFractionTool.h"
#include "JetMomentTools/JetECPSFractionTool.h"
#include "JetMomentTools/JetVertexTaggerTool.h"
#include "JetMomentTools/JetForwardJvtTool.h"
#include "JetMomentTools/JetForwardJvtToolBDT.h"
#include "JetMomentTools/JetForwardPFlowJvtTool.h"
#include "JetMomentTools/JetLArHVTool.h"
#include "JetMomentTools/JetWidthTool.h"
#include "JetMomentTools/JetMuonSegmentMomentsTool.h"

#endif

/*
  Copyright (C) 2002-2018 CERN for the benefit of the ATLAS collaboration
*/

#include "DiTauRec/ILepHadBuilder.h"
#include "DiTauRec/HadElBuilder.h"
#include "DiTauRec/HadMuBuilder.h"

#ifdef __CINT__

#pragma link off all globals;
#pragma link off all classes;
#pragma link off all functions;
#pragma link C++ nestedclass;

#endif

#ifdef __CINT__

#pragma link C++ class DiTauRec::ILepHadBuilder+;
#pragma link C++ class DiTauRec::HadElBuilder+;
#pragma link C++ class DiTauRec::HadMuBuilder+;

#endif

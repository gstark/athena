/*
  Copyright (C) 2002-2017 CERN for the benefit of the ATLAS collaboration
*/

#ifndef _VrtSecInclusive_IPAugmentor_ICC
#define _VrtSecInclusive_IPAugmentor_ICC

#include "VrtSecInclusive/VrtSecInclusive.h"
#include <numeric>

namespace VKalVrtAthena {
  
  using namespace std;
  
  //____________________________________________________________________________________________________
  template<class LeptonFlavor>
  StatusCode IPAugmentor::augmentDVimpactParametersToLeptons( const std::string& containerName )
  {
    
    const xAOD::VertexContainer *secondaryVertexContainer( nullptr );
    ATH_CHECK( evtStore()->retrieve( secondaryVertexContainer, "VrtSecInclusive_" + m_jp.secondaryVerticesContainerName ) );
    
    using LeptonContainer = DataVector<LeptonFlavor>;
    
    const LeptonContainer *leptonContainer( nullptr );
    ATH_CHECK( evtStore()->retrieve( leptonContainer, containerName ) );
    
    if( !m_decor_d0_wrtSVs    ) m_decor_d0_wrtSVs    = std::make_unique<IPDecoratorType>( "d0_wrtSVs"    + m_jp.augVerString );
    if( !m_decor_z0_wrtSVs    ) m_decor_z0_wrtSVs    = std::make_unique<IPDecoratorType>( "z0_wrtSVs"    + m_jp.augVerString );
    if( !m_decor_pt_wrtSVs    ) m_decor_pt_wrtSVs    = std::make_unique<IPDecoratorType>( "pt_wrtSVs"    + m_jp.augVerString );
    if( !m_decor_eta_wrtSVs   ) m_decor_eta_wrtSVs   = std::make_unique<IPDecoratorType>( "eta_wrtSVs"   + m_jp.augVerString );
    if( !m_decor_phi_wrtSVs   ) m_decor_phi_wrtSVs   = std::make_unique<IPDecoratorType>( "phi_wrtSVs"   + m_jp.augVerString );
    if( !m_decor_d0err_wrtSVs ) m_decor_d0err_wrtSVs = std::make_unique<IPDecoratorType>( "d0err_wrtSVs" + m_jp.augVerString );
    if( !m_decor_z0err_wrtSVs ) m_decor_z0err_wrtSVs = std::make_unique<IPDecoratorType>( "z0err_wrtSVs" + m_jp.augVerString );
    
    // Grouping decorators
    std::vector< IPDecoratorType* > decor_ipWrtSVs { m_decor_d0_wrtSVs.get(), m_decor_z0_wrtSVs.get(), m_decor_pt_wrtSVs.get(), m_decor_eta_wrtSVs.get(), m_decor_phi_wrtSVs.get(), m_decor_d0err_wrtSVs.get(), m_decor_z0err_wrtSVs.get() };
    enum { k_ip_d0, k_ip_z0, k_ip_pt, k_ip_eta, k_ip_phi, k_ip_d0err, k_ip_z0err };
    
    // Loop over leptons
    for( const auto& lepton : *leptonContainer ) {
      
      std::vector< std::vector< std::vector<float> > > ip_wrtSVs( decor_ipWrtSVs.size() ); // triple nest of { ip parameters, tracks, DVs }
      
      std::vector<unsigned> trackTypes;
      genSequence<LeptonFlavor>( lepton, trackTypes );
    
      // Loop over lepton types
      for( auto& trackType : trackTypes ) {
        
        std::vector< std::vector<float> > ip_wrtSV( decor_ipWrtSVs.size() ); // nest of { tracks, DVs }
        
        const auto* trk = getLeptonTrackParticle<LeptonFlavor>( lepton, trackType );
        
        if( !trk ) continue;
      
        std::map< const xAOD::Vertex*, std::vector<double> > distanceMap;
      
        // Loop over vertices
        for( const auto& vtx : *secondaryVertexContainer ) {
      
          std::vector<double> impactParameters;
          std::vector<double> impactParErrors;
          
          m_fitSvc->VKalGetImpact( trk, vtx->position(), static_cast<int>( lepton->charge() ), impactParameters, impactParErrors );
          
          enum { k_d0, k_z0, k_theta, k_phi, k_qOverP }; // for the impact parameter
          enum { k_d0d0, k_d0z0, k_z0z0 };               // for the par errors
          
          const auto& theta = impactParameters.at( k_theta );
          const auto& phi   = impactParameters.at( k_phi );
          const auto  p     = fabs( 1.0 / impactParameters.at(k_qOverP) );
          const auto  pt    = fabs( p * sin( theta ) );
          const auto  eta   = -log( tan(theta/2.) );
          
          // filling the parameters to the corresponding container
          ip_wrtSV.at( k_ip_d0 )    .emplace_back( impactParameters.at(k_d0) );
          ip_wrtSV.at( k_ip_z0 )    .emplace_back( impactParameters.at(k_z0) );
          ip_wrtSV.at( k_ip_pt )    .emplace_back( pt );
          ip_wrtSV.at( k_ip_eta )   .emplace_back( eta );
          ip_wrtSV.at( k_ip_phi )   .emplace_back( phi );
          ip_wrtSV.at( k_ip_d0err ) .emplace_back( impactParErrors.at(k_d0d0) );
          ip_wrtSV.at( k_ip_z0err ) .emplace_back( impactParErrors.at(k_z0z0) );
          
        } // end of vertex loop
        
        for( size_t ipar = 0; ipar < ip_wrtSVs.size(); ipar++ ) ip_wrtSVs.at( ipar ).emplace_back( ip_wrtSV.at( ipar ) );
        
      } // end of track type loop
      
      // decoration
      for( size_t ipar = 0; ipar < decor_ipWrtSVs.size(); ipar++ ) ( *( decor_ipWrtSVs.at( ipar ) ) )( *lepton ) = ip_wrtSVs.at( ipar );
      
    } // end of lepton container loop
    
    return StatusCode::SUCCESS;
  }
  
  
}


#endif

#include "GaudiKernel/LoadFactoryEntries.h"
LOAD_FACTORY_ENTRIES(VrtSecInclusive)
LOAD_FACTORY_ENTRIES(SoftBtagTrackSelector)
  //Notes:
  //
  //1. The argument to the LOAD_FACTORY_ENTRIES() is the name of the
  //   component library (libXXX.so).
  //
  // See Athena User Guide for more information

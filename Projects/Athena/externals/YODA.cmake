#
# File specifying the location of YODA to use.
#

set( YODA_LCGVERSION 1.6.5 )
set( YODA_LCGROOT
   ${LCG_RELEASE_DIR}/MCGenerators/yoda/${YODA_LCGVERSION}/${LCG_PLATFORM} )
